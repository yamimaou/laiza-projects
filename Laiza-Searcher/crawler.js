var Crawler = require("crawler");

var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            // $ is Cheerio by default
            //a lean implementation of core jQuery designed specifically for the server
            //console.log($("ol#b_results").children('li'));
            $("ol#b_results").find('li').each(function() {
              //console.log($("ol#b_results").html());
              let title = $($(this).children('div.b_title'));
              let caption = $($(this).children('div.b_caption').find('.b_attribution'));
              let body = $($(this).children('div.b_caption').children('p'));
              console.log(title.text() + ' \n ' + caption.text() + ' \n '+ body.text() +' \n ');
              //console.log($(this).text())
            })
            
        }
        done();
    }
});

// Queue just one URL, with default callback

c.queue('http://www.bing.com/search?q='+'yamitec');

// Queue a list of URLs
//c.queue(['http://www.bing.com/','http://www.yahoo.com']);
/*
// Queue URLs with custom callbacks & parameters
c.queue([{
    uri: 'http://parishackers.org/',
    jQuery: false,

    // The global callback won't be called
    callback: function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            console.log('Grabbed', res.body.length, 'bytes');
        }
        done();
    }
}]);
*/
// Queue some HTML code directly without grabbing (mostly for tests)
c.queue([{
    html: '<p>This is a <strong>test</strong></p>'
}]);