var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/yamitec');

var searchSchema = new mongoose.Schema({
  title: String,
  url: String,
  body: String,
  rate: Number
}, { collection: 'sites' }
);

module.exports = { Mongoose: mongoose, SearchSchema: searchSchema }