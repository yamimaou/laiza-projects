#### Main features
 - Multi-tab editing
 - Syntax highlighing
 - JS, CSS and HTML beautifiers
 - Toolbar show/hide
 - Some customisability
 - Full adaptation to LaizaOS UI

#### Future plans
 - Project management
 - More wide range of customizations
 - Full IDE capabilities
 - Other programming languages support